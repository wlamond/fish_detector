import numpy as np

from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from inception_v3 import preprocess_input


def unprocess_input(x):
    x /= 2.
    x += 0.5
    x *= 255.
    return x


def main():

    train_aug_settings = dict(rotation_range=40,
                              #width_shift_range=0.2,
                              #height_shift_range=0.2,
                              #shear_range=0.01,
                              #zoom_range=0.01,
                              horizontal_flip=True)
    test_file = 'data/original/ALB/img_00019.jpg'
    img = image.load_img(test_file, target_size=(299, 299))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    datagen = ImageDataGenerator(**train_aug_settings)
    #for i, batch in enumerate(datagen.flow(x, batch_size = 1,
    #                                       save_to_dir='transform_testing', save_prefix='transform_test', save_format='jpeg')):
    for i, batch in enumerate(datagen.flow(x, batch_size = 1)):
        batch = batch.reshape(3, 299, 299)
        noise = np.random.normal(0.0, 0.001, batch.shape)
        batch += noise
        # batch = preprocess_input(batch)
        batch = unprocess_input(batch)
        img_transform = image.array_to_img(batch, scale=False)
        img_transform.save('transform_testing/test{}.jpeg'.format(i))
        if i > 40:
            break



if __name__ == '__main__':
    main()
