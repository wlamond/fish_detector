import os
import sys
import time
from itertools import izip

import numpy as np
from keras import backend as K
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping
from keras.layers import Input, Dense, Dropout
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.optimizers import Adam, SGD, RMSprop
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras.utils import np_utils
from sklearn.model_selection import KFold, StratifiedKFold
from sklearn.metrics import log_loss

from inception_v3 import preprocess_input as inception_preprocess, InceptionV3


DATA_DIR = 'data'
TRAIN_DIR = os.path.join(DATA_DIR, 'original')
TEST_DIR = 'data/test_stg1'
TRAINED_MODEL_PATH = 'train_models_submission_{}'


INIT_LEARNING_RATE = 0.001
BETA_1= 0.9
SEED = 2345

CLASSES = sorted(['BET', 'DOL', 'ALB', 'YFT', 'LAG', 'SHARK', 'NoF', 'OTHER'])
TRAIN_AUG_SETTINGS = dict(rotation_range=5,
                          width_shift_range=0.2,
                          height_shift_range=0.2,
                          shear_range=0.2,
                          zoom_range=0.2,
                          horizontal_flip=True)

def read_training_data():
    start_time = time.time()
    X, y, ids = [], [], []
    for label, c in enumerate(CLASSES):
        path = os.path.join(TRAIN_DIR, c)
        for file in os.listdir(path):
            file_path = os.path.join(path, file)
            img = image.load_img(file_path, target_size=(299, 299))
            x = image.img_to_array(img)
            x = np.expand_dims(x, axis=0)
            X.append(x)
            y.append(label)
            ids.append(file)

    X = np.vstack(X)
    y = np.array(y)
    print 'time to read training data: {}'.format(time.time() - start_time)
    return X, y, ids


def read_test_data():
    start_time = time.time()
    X, ids = [], []
    for file in sorted(os.listdir(TEST_DIR)):
        file_path = os.path.join(TEST_DIR, file)
        img = image.load_img(file_path, target_size=(299, 299))
        x = image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        X.append(x)
        ids.append(file)

    X = np.vstack(X)
    print 'time to read test data: {}'.format(time.time() - start_time)
    return X, ids


def log_loss_objective(y_true, y_pred):
    clipped_y_pred = K.max(K.min(y_pred, 1.0 - 1e-15), 1e-15)
    return - K.mean(K.sum(y_true * K.log(clipped_y_pred), axis=1), axis=0)


def build_model(weights_file=None):
    input = Input(shape=(3, 299, 299))
    inception = InceptionV3(input_tensor=input)
    inception.layers.pop()
    for layer in inception.layers:
        layer.trainable = False

    l2_reg = 0.001
    output = Dense(len(CLASSES), init='glorot_normal', activation='softmax',
                   W_regularizer=l2(l2_reg), b_regularizer=l2(l2_reg))(inception.layers[-1].output)
    model = Model(input=input, output=output)

    if weights_file is not None:
        model.load_weights(weights_file)

    return model


def batch_generator(generator, preprocess_image=inception_preprocess, add_noise=True):
    """Apply the given preprocessing function to each batch generated by generator."""
    for X, y in generator:
        if add_noise:
            noise = np.random.normal(0.0, 0.01, X.shape)
        else:
            noise = np.zeros(shape=X.shape)
        yield (preprocess_image(X) + noise, y)


def make_learning_rate_schedule(init_learning_rate=INIT_LEARNING_RATE, reduction_rate=2, reduction_steps=5):

    def learning_rate_schedule(epoch):
        return init_learning_rate / (reduction_rate ** (epoch / reduction_steps))

    return learning_rate_schedule


def train_models(n_folds, submission_id, backprop_further=False):
    """Train n_folds models and write the weights to files on disk."""
    start_time = time.time()
    X, y, ids = read_training_data()

    trained_model_path = TRAINED_MODEL_PATH.format(submission_id)
    if not os.path.isdir(trained_model_path):
        os.mkdir(trained_model_path)

    if n_folds < 2:
        cv = StratifiedKFold(n_splits=10, shuffle=True, random_state=SEED)
    else:
        cv = StratifiedKFold(n_splits=n_folds, shuffle=True, random_state=SEED)

    train_datagen = ImageDataGenerator(**TRAIN_AUG_SETTINGS)
    val_datagen = ImageDataGenerator() # don't apply transforms for val set
    models = []
    val_losses = []

    for fold, (train_slice, val_slice) in enumerate(cv.split(X, y)):
        print 'starting fold {}'.format(fold)
        X_train = X[train_slice]
        y_train = np_utils.to_categorical(y[train_slice], len(CLASSES))
        X_val = X[val_slice]
        y_val = np_utils.to_categorical(y[val_slice], len(CLASSES))

        model = build_model()

        adam = Adam(lr=INIT_LEARNING_RATE, beta_1=BETA_1)
        model.compile(optimizer=adam, loss='categorical_crossentropy', metrics=[log_loss_objective])
        best_model_file = os.path.join(trained_model_path,
                                       'inception_weights_fold_{fold}.hdf5'.format(fold=fold))

        train_gen = batch_generator(train_datagen.flow(X_train, y_train, batch_size=100, seed=SEED), add_noise=True)
        val_gen = batch_generator(val_datagen.flow(X_val, y_val, batch_size=100, seed=SEED), add_noise=False)

        # train twice: once with just the last layer and a second time with some more layers
        #learning_rate_schedule = make_learning_rate_schedule(init_learning_rate=0.001, reduction_rate=1, reduction_steps=10)
        model.fit_generator(train_gen, 6000, 30,
                            callbacks=[ModelCheckpoint(best_model_file, save_best_only=True)],
                                       #LearningRateScheduler(learning_rate_schedule)],
                            validation_data=val_gen, nb_val_samples=350,
                            nb_worker=3, pickle_safe=True)
        model.load_weights(best_model_file)
        y_pred = model.predict(inception_preprocess(X_val), batch_size=50)
        val_loss = log_loss(y_val, y_pred)
        print '{} fold val loss for top layer: {}'.format(fold, val_loss)

        if backprop_further:
            # relax the fixed layer constraints
            # 173, 195 are good picks
            for layer in model.layers[195:]:
                layer.trainable = True

            further_learning_rate = INIT_LEARNING_RATE / 10
            model.compile(optimizer=Adam(lr=further_learning_rate, beta_1=BETA_1), loss='categorical_crossentropy', metrics=[log_loss_objective])

            learning_rate_schedule = make_learning_rate_schedule(init_learning_rate=further_learning_rate, reduction_rate=2, reduction_steps=5)
            model.fit_generator(train_gen, 6000, 30,
                                callbacks=[ModelCheckpoint(best_model_file, save_best_only=True),],
                                           #LearningRateScheduler(learning_rate_schedule)],
                                validation_data=val_gen, nb_val_samples=350,
                                nb_worker=3, pickle_safe=True)
            model.load_weights(best_model_file)
            y_pred = model.predict(inception_preprocess(X_val), batch_size=50)
            val_loss = log_loss(y_val, y_pred)
            print '{} fold val loss for more layers: {}'.format(fold, val_loss)

        val_losses.append(val_loss)
        models.append(model)
        if fold == n_folds - 1:
            break

    print 'final val score:', np.mean(val_losses)
    print 'total training time: {}'.format(time.time() - start_time)
    return models


def compute_test_predictions(submission_id, models, n_transform_samples=50):
    """Sample many transformations from each test image and average the predictions from each model."""
    X, ids = read_test_data()
    start_time = time.time()
    all_predictions = []
    datagen = ImageDataGenerator(**TRAIN_AUG_SETTINGS)
    for i, (x, id) in enumerate(izip(X, ids)):
        if i < 10 or i % 10 == 0:
            print 'progress:', i
        x_p = np.expand_dims(inception_preprocess(x), axis=0)
        sampled_x_t = []
        x_t = next(datagen.flow(x_p, batch_size=n_transform_samples))
        predictions = [np.mean(model.predict(x_t), axis=0) for model in models]
        prediction = np.mean(np.vstack(predictions), axis=0)
        all_predictions.append((id, prediction.tolist()))

    print 'total time to compute test predictions: {}'.format(time.time() - start_time)
    return all_predictions


def write_submission_file(submission_id, test_predictions):
    """Write a submission file given a submission ID and predictions."""
    with open('submission_{}.csv'.format(submission_id), 'w') as sub_file:
        sub_file.write('image,ALB,BET,DOL,LAG,NoF,OTHER,SHARK,YFT\n')
        for id, prediction in test_predictions:
            line = ','.join([id] + map(str, prediction)) + '\n'
            sub_file.write(line)


def load_submission_models(submission_id):
    """Load models generated during the given submission ID."""
    trained_model_path = TRAINED_MODEL_PATH.format(submission_id)
    if not os.path.isdir(trained_model_path):
        print 'no models trained for that submission id.'
        sys.exit()

    models = []
    model_files = os.listdir(trained_model_path)
    for file in model_files:
        print 'loading model', file
        weights_file = os.path.join(trained_model_path, file)
        model = build_model(weights_file)
        models.append(model)

    return models


def main():
    start_time = time.time()
    submission_id = 19
    models = train_models(1, submission_id)
    # optional in case we need to recompute results from a submission configuration
    # models = load_submission_models(submission_id)
    test_predictions = compute_test_predictions(submission_id, models, n_transform_samples=1)
    write_submission_file(submission_id, test_predictions)
    print 'total time to train and compute predictions:', time.time() - start_time


if __name__ == '__main__':
    main()
