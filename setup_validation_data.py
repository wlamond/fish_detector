import os

from random import random
from shutil import copyfile


DATA_DIR = 'data'
ORIGINAL_DIR = os.path.join(DATA_DIR, 'original')
TRAIN_DIR = os.path.join(DATA_DIR, 'train')
VAL_DIR = os.path.join(DATA_DIR, 'val')


def safe_mkdir(path):
    if not os.path.isdir(path):
        os.mkdir(path)


def main():

    safe_mkdir(TRAIN_DIR)
    safe_mkdir(VAL_DIR)

    # get all types
    # for each type, for each file, copy to train or val based on prng

    types = os.listdir(ORIGINAL_DIR)
    print types

    for type in types:
        if type == '.DS_Store':
            continue

        train_path = os.path.join(TRAIN_DIR, type)
        val_path = os.path.join(VAL_DIR, type)
        safe_mkdir(train_path)
        safe_mkdir(val_path)

        type_dir = os.path.join(ORIGINAL_DIR, type)
        files = os.listdir(type_dir)
        for file in files:
            r = random()

            if r < 0.9:
                dest = os.path.join(train_path, file)
            else:
                dest = os.path.join(val_path, file)

            copyfile(os.path.join(type_dir, file), dest)


if __name__ == '__main__':
    main()
